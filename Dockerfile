FROM swift:5.1-slim

WORKDIR /app
COPY viveiro /app

ENV HOSTNAME=0.0.0.0
ENV PORT=80

EXPOSE $PORT

ENTRYPOINT ./viveiro serve --env $ENVIRONMENT --hostname $HOSTNAME --port $PORT
