# CHANGELOG

<!--- next entry here -->

## 0.1.0
2019-09-18

### Features

- **backend-213:** Project setup 🚀 (c88ece8c7959dfdaf579621f919311b1c8e45ad4)
- **backend-214:** Swagger (48d84f72486dc185e772c59038870428a3f86678)
- **backend-213:** Helm Chart submodule and gitlab-ci file (6c546927a92566993bfbf31f67d2fd708ccb7eae)
- **backend-213:** HealthCheck and refactor (61800370e8e9504be3117cf8ec5880f48bc5dbd2)
- **backend-218:** CRUD operations (7be7350c2f98a4b8dbf4ca7a362f2ecbccd3848d)
- **backend-222:** Inject service configs through Helm (a698da2506faf0724cc9ad4b5552138d077751aa)
- **backend-215:** Mexico for groups (3f6cf1d5cea7cbde1ce4fbed85752eb8167aabb6)