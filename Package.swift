// swift-tools-version:4.0
import PackageDescription

let package = Package(
    name: "viveiro",
    dependencies: [
        .package(url: "https://github.com/youclap/vapor-shared.git", from: "0.6.0")
    ],
    targets: [
        .target(name: "App", dependencies: ["YouClap"]),
        .target(name: "Run", dependencies: ["App"])
    ]
)
