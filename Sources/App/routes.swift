import Vapor
import YouClap

/// Register your application's routes here.
public func routes(_ router: Router, _ container: Container) throws {
    // Create an instance of your controller, for example an HTTP one and register it in the router

    try router.register(collection: HealthCheckController())

    let groupService = try container.make(GroupServiceRepresentable.self)
    try router.register(collection: GroupHTTPController(service: groupService))

    let mexicoService = try container.make(MexicoServiceRepresentable.self)
    try router.register(collection: MexicoHTTPController(service: mexicoService))

}
