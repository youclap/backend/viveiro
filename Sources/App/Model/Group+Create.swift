import Fluent
import Vapor
import YouClap

extension Group {
    struct Create {
        var name: String
        var createdByProfileID: Profile.ID
        var createdByUserID: User.ID
//        var privacy: Group.Privacy
        var members: [Profile.ID]
    }
}

extension Group.Create: Content {
    enum Key: CodingKey {
        case name
        case createdByProfileID
        case createdByUserID
        case privacy
        case members
    }

    enum CodingError: Error {
        case unknownValue
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: Key.self)

        self.name = try container.decode(String.self, forKey: .name)
        self.createdByProfileID = try container.decode(Profile.ID.self, forKey: .createdByProfileID)
        self.createdByUserID = try container.decode(User.ID.self, forKey: .createdByUserID)
        self.members = try container.decode([UnsignedBigInt].self, forKey: .members)
    }
}
