import Vapor

extension Group {
    struct Response {
        let id: ID
        let name: String
        let createdDate: Date
        let createdByProfileID: Profile.ID
        let createdByUserID: User.ID // TODO not sure if needed
        var privacy: String
        var firebaseID: String?
    }
}

extension Group.Response: Content {
}

extension Group.Response {
    init(group: Group) throws {
        self.id = try group.requireID()
        self.name = group.name
        // swiftlint:disable:next force_unwrapping
        self.createdDate = group.createdDate!
        self.createdByProfileID = group.createdByProfileID
        self.createdByUserID = group.createdByUserID
        self.privacy = group.privacy.rawValue
        self.firebaseID = group.firebaseID
    }
}
