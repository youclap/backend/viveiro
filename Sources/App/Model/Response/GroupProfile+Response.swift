import Vapor

extension GroupProfile {
    struct Response {
        let groupID: Group.ID
        let profileID: Profile.ID
        let roleID: Int
        let createdByProfileID: Profile.ID
        let createdByUserID: User.ID
        let createdDate: Date
    }
}

extension GroupProfile.Response: Content {
}

extension GroupProfile.Response {
    init(groupProfile: GroupProfile) throws {
        // swiftlint:disable:next force_unwrapping
        self.groupID = groupProfile.groupID!
        self.profileID = groupProfile.profileID
        self.roleID = groupProfile.roleID
        self.createdByProfileID = groupProfile.createdByProfileID
        self.createdByUserID = groupProfile.createdByUserID
        // swiftlint:disable:next force_unwrapping
        self.createdDate = groupProfile.createdDate!
    }
}
