import Vapor

struct MexicoGroupMember {
    let addedBy: String? = nil
    let deleted: Bool
    let groupID: String
    let timestamp: Date
    let userID: String
}

extension MexicoGroupMember: Content {}
