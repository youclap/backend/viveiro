import FluentMySQL
import Vapor
import YouClap

struct Group {
    typealias ID = UnsignedBigInt

    var id: ID?
    var name: String
    var createdDate: Date?
    var createdByProfileID: Profile.ID
    var createdByUserID: User.ID
    var privacyID: Int
    var firebaseID: String
    var deleted: Bool

    init(id: ID? = nil,
         name: String,
         createdDate: Date? = nil,
         createdByProfileID: Profile.ID,
         createdByUserID: User.ID,
         privacyID: Int = Group.Privacy.private.id,
         firebaseID: String,
         deleted: Bool = false) {
        self.id = id
        self.name = name
        self.createdDate = createdDate
        self.createdByProfileID = createdByProfileID
        self.createdByUserID = createdByUserID
        self.privacyID = privacyID
        self.firebaseID = firebaseID
        self.deleted = deleted
    }
}

extension Group {
    var privacy: Privacy { .init(privacyID) }
}

extension Group: MySQLUnsignedBigIntModel {
    static let createdAtKey: TimestampKey? = \.createdDate
}

extension Group: Content {}
