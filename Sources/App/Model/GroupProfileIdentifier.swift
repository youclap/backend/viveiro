import Foundation
import YouClap

struct GroupProfileIdentifier {
    let groupID: Group.ID
    let groupFirebaseIdentifier: String
    let profileID: UnsignedBigInt
    let profileFirebaseIdentifier: String
}

extension GroupProfileIdentifier: Decodable {}
