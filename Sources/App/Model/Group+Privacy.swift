import FluentMySQL
import Vapor

extension Group {
    enum Privacy: String {
        case `public` = "PUBLIC"
        case `private` = "PRIVATE"
    }
}

extension Group.Privacy {
    var id: Int {
        switch self {
        case .public: return 0
        case .private: return 1
        }
    }

    init(_ id: Int) {
        switch id {
        case 0: self = .public
        case 1: self = .private
        default: fatalError("Unknown value")
        }
    }
}

extension Group.Privacy: Codable {
    enum Key: CodingKey {
        case rawValue
    }

    enum CodingError: Error {
        case unknownValue
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: Key.self)
        let rawValue = try container.decode(Int.self, forKey: .rawValue)

        switch rawValue {
        case 0: self = .public
        case 1: self = .private
        default: throw CodingError.unknownValue
        }
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: Key.self)
        try container.encode(id, forKey: .rawValue)
    }
}
