import FluentMySQL
import Vapor

extension GroupProfile {
    enum Role: String, RawRepresentable {
        case admin = "ADMIN"
        case member = "MEMBER"
    }
}

extension GroupProfile.Role: MySQLModel {
    static var sqlTableIdentifierString: String { "GroupRole" }

    static var entity: String { "GroupRole" }

    var id: Int? {
        get {
            switch self {
            case .admin: return 0
            case .member: return 1
            }
        }
        set(newValue) {
            switch newValue {
            case 0?: self = .admin
            case 1?: self = .member
            default: assertionFailure("💥 value \(String(describing: newValue)) not handled.")
            }
        }
    }

    init(_ id: Int) {
        switch id {
        case 0: self = .admin
        case 1: self = .member
        default: fatalError("Unknown value")
        }
    }
}

extension GroupProfile.Role: Codable {
    enum Key: CodingKey {
        case id
    }

    enum CodingError: Error {
        case unknownValue
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: Key.self)
        let rawValue = try container.decode(Int.self, forKey: .id)

        switch rawValue {
        case 0: self = .admin
        case 1: self = .member
        default: throw CodingError.unknownValue
        }
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: Key.self)
        try container.encode(id, forKey: .id)
    }
}
