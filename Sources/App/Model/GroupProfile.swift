import FluentMySQL
import Vapor

struct GroupProfile {
    var id: Int?
    var groupID: Group.ID?
    var profileID: Profile.ID
    var roleID: Int
    var createdByProfileID: Profile.ID
    var createdByUserID: User.ID
    var createdDate: Date?
    var deleted: Bool

    init(id: Int? = nil,
         groupID: Group.ID? = nil,
         profileID: Profile.ID,
         roleID: Int = GroupProfile.Role.member.id!, // swiftlint:disable:this force_unwrapping
         createdByProfileID: Profile.ID,
         createdByUserID: User.ID,
         createdDate: Date? = nil,
         deleted: Bool = false) {
        self.id = id
        self.groupID = groupID
        self.profileID = profileID
        self.roleID = roleID
        self.createdByProfileID = createdByProfileID
        self.createdByUserID = createdByUserID
        self.createdDate = createdDate
        self.deleted = deleted
    }
}

extension GroupProfile {
    var role: Role { .init(roleID) }
}

extension GroupProfile: MySQLModel {
    static let createdAtKey: TimestampKey? = \.createdDate

    static var name: String { "Group_Profile" }
}
