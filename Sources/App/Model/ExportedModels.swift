import YouClap

enum Profile {
    typealias ID = UnsignedBigInt
}

enum User {
    typealias ID = UnsignedBigInt
}
