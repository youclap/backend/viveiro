import Vapor

struct MexicoGroup {
    let creatorID: String
    let name: String
    let timestamp: Date
    let deleted: Bool = false
    let uid: String
}

extension MexicoGroup: Content {}
