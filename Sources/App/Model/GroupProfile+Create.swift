import Vapor
import YouClap

extension GroupProfile {
    struct Create {
        var groupID: Group.ID
        var profileID: Profile.ID
//        var role: GroupProfile.Role
        var createdByProfileID: Profile.ID
        var createdByUserID: User.ID
    }
}
