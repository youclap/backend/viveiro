import FluentMySQL
import GoogleCloudKit
import GoogleCloudPubSubKit
import Vapor
import YouClap

// Called before your application initializes.
// swiftlint:disable:next function_body_length
public func configure(_ env: Environment) throws -> (Config, Services) {
    let config = Config.default()
    var services = Services.default()

    services.register(contentConfig())

    try services.register(FluentMySQLProvider())

    try services.register(GoogleCloudPubSubProvider())

    // Register middleware
    var middlewares = MiddlewareConfig() // Create _empty_ middleware config
    middlewares.use(CORSMiddleware(configuration: corsConfiguration()))
    middlewares.use(ErrorMiddleware.self) // Catches errors and converts to HTTP response
    services.register(middlewares)

    // Configure MySQL
    guard let mysqlDatabaseConfig = MySQLDatabaseConfig.loadFromEnvironment() else {
        throw ConfigurationError.database("💥 missing required arguments")
    }

    var databaseConfig = DatabasesConfig()
    databaseConfig.enableLogging(on: .mysql)
    databaseConfig.add(database: MySQLDatabase(config: mysqlDatabaseConfig), as: .mysql)
    services.register(databaseConfig)

    guard
        let databaseMaxConnectionsAsString = Environment.get("DATABASE_CONNECTIONS"),
        let databaseMaxConnections = Int(databaseMaxConnectionsAsString)
        else { throw ConfigurationError.database("💥 missing number of connections") }

    services.register(DatabaseConnectionPoolConfig(maxConnections: databaseMaxConnections))

    services.register(GoogleCloudCredentialsConfiguration.self) { _ in
        GoogleCloudCredentialsConfiguration(credentialsFile: "service-account.json")
    }

    services.register(PubSubConfiguration.self) { _ in
        PubSubConfiguration(scope: [.fullControl], serviceAccount: "default")
    }

    services.register(Store.self) { container -> Store in
        let mysqlDatabaseConnectable = MySQLDatabaseConnectable(container: container)
        let messageQueueClient = try container.make(PublisherClient.self)
        return Store(databaseConnectable: mysqlDatabaseConnectable, messageQueueClient: messageQueueClient)
    }

    services.register(AmericaServiceRepresentable.self) { container -> AmericaService<Store> in
        let store = try container.make(Store.self)
        let logger = try container.make(Logger.self)
        return AmericaService(store: store, logger: logger)
    }

    services.register(GroupServiceRepresentable.self) { container -> GroupService<Store> in
        let logger = try container.make(Logger.self)
        let store = try container.make(Store.self)
        let americaService = try container.make(AmericaServiceRepresentable.self)
        return GroupService(logger: logger, store: store, americaService: americaService)
    }

    services.register(MexicoServiceRepresentable.self) { container -> MexicoService<Store> in
        let store = try container.make(Store.self)
        return MexicoService(store: store)
    }

    // Register routes to the router
    services.register(Router.self) { container -> EngineRouter in
        let router = EngineRouter.default()
        try routes(router, container)
        return router
    }

    return (config, services)
}

enum ConfigurationError: Error {
    case database(String)
}

private func contentConfig() -> ContentConfig {
    var config = ContentConfig()

    // json
    let encoder = JSONEncoder()
    let decoder = JSONDecoder()

    encoder.dateEncodingStrategy = .formatted(.iso8601)
    decoder.dateDecodingStrategy = .formatted(.iso8601)

    config.use(encoder: encoder, for: .json)
    config.use(decoder: decoder, for: .json)

    config.use(decoder: URLEncodedFormDecoder(), for: .urlEncodedForm)

    return config
}

private func corsConfiguration() -> CORSMiddleware.Configuration {
    CORSMiddleware.Configuration(
        allowedOrigin: .all,
        allowedMethods: [.GET, .POST, .PUT, .OPTIONS, .DELETE, .PATCH],
        allowedHeaders: [
            .authToken,
            .contentType,
            .userAgent,
            .accessControlAllowOrigin
        ]
    )
}

extension HTTPHeaderName {
    static let authToken = HTTPHeaderName("Auth-Token")
}
