import FluentMySQL
import Vapor

enum GroupStoreError: Error {
    case groupNotFound(Group.ID)
    case groupProfileNotFound(Group.ID, Profile.ID)
    case groupProfileRole(Group.ID, Profile.ID)
    case sql(Error)
}

protocol GroupStore {
    func create(group: Group, members: [GroupProfile]) -> Future<(Group, [GroupProfile])>
    func group(for id: Group.ID, authenticatedProfileID: Profile.ID) -> Future<Group>
    func update(group: Group) -> Future<Group>
    func delete(group groupID: Group.ID) -> Future<Void>

    func members(groupID: Group.ID) -> Future<[GroupProfile]>
    func addMember(groupProfile: GroupProfile) -> Future<GroupProfile>
    func removeMember(groupID: Group.ID, profileID: Profile.ID) -> Future<Void>
    func groupsForProfileID(profileID: Profile.ID) -> Future<[Group.Response]>

    func group(_ group: Group, containsProfileID profileID: Profile.ID) -> Future<Bool>
    func role(for profileID: Profile.ID, groupID: Group.ID) -> Future<GroupProfile.Role>
}

extension Store: GroupStore {

    func create(group: Group, members: [GroupProfile]) -> Future<(Group, [GroupProfile])> {
        databaseConnectable.transaction { connection in
            group.save(on: connection)
                .flatMap { group in
                    let futures = members.map { member -> Future<GroupProfile> in
                        var member = member
                        member.groupID = group.id

                        return member.save(on: connection)
                    }

                    return Future.whenAll(futures, eventLoop: connection.eventLoop).map { (group, $0) }
                }
        }
    }

    func group(for id: Group.ID, authenticatedProfileID: Profile.ID) -> Future<Group> {
        databaseConnectable.connection {
            // swiftlint:disable:next first_where
            Group.query(on: $0)
                .join(\GroupProfile.groupID, to: \Group.id, method: .inner)
                .filter(\Group.id, .equal, id)
                .filter(\GroupProfile.profileID, .equal, authenticatedProfileID)
                .first()
                .map {
                    guard let group = $0 else {
                        throw GroupStoreError.groupNotFound(id)
                    }

                    return group
                }
        }
    }

    func update(group: Group) -> Future<Group> {
        databaseConnectable.connection {
            group.update(on: $0)
        }
    }

    func delete(group groupID: Group.ID) -> Future<Void> {
        databaseConnectable.transaction { connection in

           let groupFuture = Group.query(on: connection)
                .filter(\.id == groupID)
                .update(\.deleted, to: true)
                .run()

            let membersFuture = GroupProfile.query(on: connection)
                .filter(\.groupID == groupID)
                .update(\.deleted, to: true)
                .run()

            return groupFuture.and(membersFuture)
                .map { _ in () }
        }
    }

    func members(groupID: Group.ID) -> Future<[GroupProfile]> {
        databaseConnectable.connection {
            GroupProfile.query(on: $0)
                .filter(\.groupID == groupID)
                .filter(\.deleted == false)
                .all()
        }
    }

    func addMember(groupProfile: GroupProfile) -> Future<GroupProfile> {
        databaseConnectable.connection {
            groupProfile.save(on: $0)
        }
    }

    func removeMember(groupID: Group.ID, profileID: Profile.ID) -> Future<Void> {
        databaseConnectable.connection {
            GroupProfile.query(on: $0)
                .filter(\.groupID == groupID)
                .filter(\.profileID == profileID)
                .filter(\.deleted == false)
                .delete()
        }
    }

    func groupsForProfileID(profileID: Profile.ID) -> Future<[Group.Response]> {
        databaseConnectable.connection {
            $0.raw("""
                SELECT Gr.*, GroupPrivacy.name as privacy
                FROM `Group` as Gr
                INNER JOIN Group_Profile on Gr.id = Group_Profile.groupID
                INNER JOIN GroupPrivacy on GroupPrivacy.id = Gr.privacyID
                WHERE Gr.deleted=0 AND Group_Profile.profileID=?;
            """)
                .bind(profileID)
                .all(decoding: Group.Response.self)
        }
    }

    func group(_ group: Group, containsProfileID profileID: Profile.ID) -> EventLoopFuture<Bool> {
        databaseConnectable.connection {
            // swiftlint:disable:next first_where
            GroupProfile.query(on: $0)
                .filter(\.profileID, .equal, profileID)
                .first()
                .map { $0 == nil ? false : true }
        }
    }

    func role(for profileID: Profile.ID, groupID: Group.ID) -> EventLoopFuture<GroupProfile.Role> {
        databaseConnectable.connection {
            // swiftlint:disable:next first_where
            GroupProfile.Role.query(on: $0)
                .join(\GroupProfile.roleID, to: \GroupProfile.Role.id, method: .inner)
                .filter(\GroupProfile.groupID == groupID)
                .filter(\GroupProfile.profileID == profileID)
                .first()
                .map {
                    guard let groupRole = $0 else {
                        throw GroupStoreError.groupProfileRole(groupID, profileID)
                    }

                    return groupRole
                }
        }
    }
}
