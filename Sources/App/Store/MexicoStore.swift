import FluentMySQL
import Vapor

enum MexicoStoreError: Error {
    case groupNotFound(String)
    case profileNotFound(String)
    case userNotFound(String)
    case invalidFirebaseID
    case sql(Error)
}

protocol MexicoStore: Service {

    func readGroup(firebaseID: String) -> Future<Group>
    func readProfileID(firebaseID: String?) -> Future<Profile.ID>
    func readUserID(firebaseID: String?) -> Future<User.ID>
}

extension Store: MexicoStore {

    func readGroup(firebaseID: String) -> Future<Group> {

        databaseConnectable.connection { connection in

            // swiftlint:disable:next first_where
            Group.query(on: connection)
                .filter(\.firebaseID == firebaseID)
                .first()
                .map {
                    guard let group = $0 else {
                        throw MexicoStoreError.groupNotFound(firebaseID)
                    }

                    return group
                }
        }
    }

    func readProfileID(firebaseID: String?) -> Future<Profile.ID> {

        guard let firebaseID = firebaseID else {
            return databaseConnectable.eventLoop.newFailedFuture(error: MexicoStoreError.invalidFirebaseID)
        }

        return databaseConnectable.connection { connection in

            connection.raw("SELECT id FROM Profile INNER JOIN FirebaseIdentifier ON profileID = id WHERE firebaseID = ?")
                .bind(firebaseID)
                .first()
                .map {
                    guard
                        let data = $0,
                        let value = data.values.first,
                        let profileID = try value.integer(Profile.ID.self)
                    else {
                        throw MexicoStoreError.profileNotFound(firebaseID)
                    }

                    return profileID
                }
        }
    }

    func readUserID(firebaseID: String?) -> Future<User.ID> {

        guard let firebaseID = firebaseID else {
            return databaseConnectable.eventLoop.newFailedFuture(error: MexicoStoreError.invalidFirebaseID)
        }

        return databaseConnectable.connection { connection in

            connection.raw("""
                    SELECT id FROM User INNER JOIN AuthenticationIdentifier ON userID = id WHERE authenticationID = ?
                    """)
                .bind(firebaseID)
                .first()
                .map {
                    guard
                        let data = $0,
                        let value = data.values.first,
                        let userID = try value.integer(User.ID.self)
                        else {
                            throw MexicoStoreError.userNotFound(firebaseID)
                    }

                    return userID
                }
        }
    }
}
