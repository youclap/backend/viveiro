import protocol Vapor.Service
import NIO

enum AmericaGroupStoreError: Error {
    case notFoundGroup(Group.ID)
    case notFoundGroupAndProfile(Group.ID, Profile.ID)
    case missingGroupForGroupProfile(GroupProfile)
}

protocol AmericaGroupStore {
    func firebaseIdentifier(for groupID: Group.ID) -> EventLoopFuture<String>
    func firebaseIdentifier(for groupID: Group.ID, profileID: Profile.ID) -> EventLoopFuture<GroupProfileIdentifier>
    func firebaseIdentifier(for groupProfile: GroupProfile) -> EventLoopFuture<GroupProfileIdentifier>
    func firebaseIdentifiers(for groupsProfile: [GroupProfile]) -> EventLoopFuture<[GroupProfileIdentifier]>
}

extension Store: AmericaGroupStore {

    func firebaseIdentifier(for groupID: Group.ID) -> EventLoopFuture<String> {
        databaseConnectable.connection {
            Group.find(groupID, on: $0)
                .map {
                    guard let group = $0 else {
                        throw AmericaGroupStoreError.notFoundGroup(groupID)
                    }

                    return group.firebaseID
                }
        }
    }

    func firebaseIdentifier(for groupID: Group.ID, profileID: Profile.ID) -> EventLoopFuture<GroupProfileIdentifier> {
        databaseConnectable.connection { connection in
            let sqlString = """
                SELECT G.id as groupID,
                       G.firebaseID as groupFirebaseIdentifier,
                       FI.profileID as profileID,
                       FI.firebaseID as profileFirebaseIdentifier
                FROM (
                      ((SELECT id, firebaseID FROM `Group`
                      WHERE id = \(groupID)) as G),
                      ((SELECT profileID, firebaseID FROM `FirebaseIdentifier`
                      WHERE profileID = \(profileID)) as FI)
                     )
            """

            return connection.raw(sqlString)
                .first(decoding: GroupProfileIdentifier.self)
                .map {
                    guard let groupProfileFirebaseIdentifier = $0 else {
                        throw AmericaGroupStoreError.notFoundGroupAndProfile(groupID, profileID)
                    }

                    return groupProfileFirebaseIdentifier
                }
        }
    }

    func firebaseIdentifier(for groupProfile: GroupProfile) -> EventLoopFuture<GroupProfileIdentifier> {
        guard let groupID = groupProfile.groupID else {
            return databaseConnectable.future(error: AmericaGroupStoreError.missingGroupForGroupProfile(groupProfile))
        }

        return firebaseIdentifier(for: groupID, profileID: groupProfile.profileID)
    }

    func firebaseIdentifiers(for groupsProfile: [GroupProfile]) -> EventLoopFuture<[GroupProfileIdentifier]> {
        databaseConnectable.connection { connection in
            let filteredGroups = groupsProfile.filter { $0.groupID != nil }

            let groupIDs = filteredGroups.compactMap { $0.groupID.flatMap { "\($0)" } }
            let profileIDs = Set(
                filteredGroups.map { "\($0.profileID)" } +
                filteredGroups.map { "\($0.createdByProfileID)" })

            let sqlString = """
                SELECT DISTINCT G.id as groupID,
                                G.firebaseID as groupFirebaseIdentifier,
                                P.profileID as profileID,
                                P.firebaseID as profileFirebaseIdentifier
                FROM (
                      ((SELECT id, firebaseID FROM `Group`
                      WHERE id IN (\(groupIDs.map { _ in "?" }.joined(separator: ",")))
                      ORDER BY FIELD (id, \(groupIDs.map { _ in "?" }.joined(separator: ",")))) as G),
                      ((SELECT profileID, firebaseID FROM `FirebaseIdentifier`
                      WHERE profileID IN (\(profileIDs.map { _ in "?" }.joined(separator: ",")))
                      ORDER BY FIELD (profileID, \(profileIDs.map { _ in "?" }.joined(separator: ",")))) as P)
                     )
                ORDER BY FIELD (G.id, \(groupIDs.map { _ in "?" }.joined(separator: ",")))
            """

            return connection.raw(sqlString)
                .binds(groupIDs + groupIDs + profileIDs + profileIDs + groupIDs)
                .all(decoding: GroupProfileIdentifier.self)
        }
    }
}
