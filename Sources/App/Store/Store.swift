import GoogleCloudPubSubKit
import Vapor
import YouClap

final class Store: Service {
    let databaseConnectable: MySQLDatabaseConnectable
    let messageQueueClient: PublisherClient

    init(databaseConnectable: MySQLDatabaseConnectable,
         messageQueueClient: PublisherClient) {
        self.databaseConnectable = databaseConnectable
        self.messageQueueClient = messageQueueClient
    }
}

extension Store: AmericaStore {}
