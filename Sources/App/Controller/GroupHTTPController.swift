import Vapor
import YouClap

final class GroupHTTPController: RouterController {
    private let service: GroupServiceRepresentable

    init(service: GroupServiceRepresentable) {
        self.service = service
    }

    func boot(router: Router) throws {
        let group = router.grouped("group")

        group.post(Group.Create.self, at: "/", use: create)
        group.get("/", Group.ID.parameter, use: readForID)
        group.delete("/", Group.ID.parameter, use: delete)

        group.get("/", Group.ID.parameter, "members", use: members)
        group.post("/", Group.ID.parameter, "member", Profile.ID.parameter, use: addMember)
        group.delete("/", Group.ID.parameter, "member", Profile.ID.parameter, use: removeMember)

        let groups = router.grouped("groups")
        groups.get("/", use: retrieveGroups)
    }

    // MARK: - CRUD Methods

    private func retrieveGroups(_ request: Request) throws -> Future<[Group.Response]> {
        guard let profileID = try? request.query.get(Profile.ID.self, at: "profileID") else {
            throw Abort(.badRequest)
        }
        _ = try request.authenticationID()

        return service.groups(for: profileID)
    }

    private func create(_ request: Request, group: Group.Create) throws -> Future<Response> {
        _ = try request.authenticationID()

        return try service.create(group: group)
            .encode(status: .created, for: request)
    }

    private func readForID(_ request: Request) throws -> Future<Group.Response> {
        _ = try request.authenticationID()

        let authenticatedProfileID = try request.profileID()
        let groupID = try self.groupID(from: request)

        return service.group(for: groupID, authenticatedProfileID: authenticatedProfileID)
    }

    private func delete(_ request: Request) throws -> Future<HTTPStatus> {
        _ = try request.authenticationID()

        let authenticatedProfileID = try request.profileID()
        let groupID = try self.groupID(from: request)

        return service.delete(group: groupID, authenticatedProfileID: authenticatedProfileID)
            .transform(to: .noContent)
    }

    private func members(_ request: Request)throws -> Future<[Profile.ID]> {
        _ = try request.authenticationID()

        let authenticatedProfileID = try request.profileID()
        let groupID = try self.groupID(from: request)

        return service.members(groupID: groupID, authenticatedProfileID: authenticatedProfileID)
    }

    private func addMember(_ request: Request)throws -> Future<Response> {
        _ = try request.authenticationID()

        let authenticatedProfileID = try request.profileID()
        let authenticatedUserID = try request.userID()

        let groupID = try self.groupID(from: request)
        let profileID = try self.profileID(from: request)

        let groupProfileCreate = GroupProfile.Create(groupID: groupID,
                                                     profileID: profileID,
                                                     createdByProfileID: authenticatedProfileID,
                                                     createdByUserID: authenticatedUserID)

        return service.addMember(groupProfile: groupProfileCreate, authenticatedProfileID: authenticatedProfileID)
            .encode(status: .created, for: request)
    }

    private func removeMember(_ request: Request)throws -> Future<HTTPStatus> {
        _ = try request.authenticationID()

        let authenticatedProfileID = try request.profileID()

        let groupID = try self.groupID(from: request)
        let profileID = try self.profileID(from: request)

        guard authenticatedProfileID != profileID else { throw Abort(.unauthorized) }

        return service.removeMember(from: groupID, member: profileID, authenticatedProfileID: authenticatedProfileID)
            .transform(to: .noContent)
    }

    // MARK: - Private Methods

    private func groupID(from request: Request) throws -> Group.ID {
        guard let groupID = try? request.parameters.next(Group.ID.self) else {
            throw Abort(.badRequest)
        }
        return groupID
    }

    private func profileID(from request: Request) throws -> Profile.ID {
        guard let profileID = try? request.parameters.next(Profile.ID.self) else {
            throw Abort(.badRequest)
        }
        return profileID
    }
}

// MARK: - Debuggables

extension GroupServiceError: AbortError {
    var status: HTTPResponseStatus {
        switch self {
        case .groupAlreadyContainsProfile: return .badRequest
        case .groupNotFound: return .notFound
        case .groupProfileNotFound: return .notFound
        case .unauthorizedToDeleteGroup: return .unauthorized
        case .unauthorizedToRemoveProfileFromGroup: return .unauthorized
        case .store: return .internalServerError
        }
    }

    var identifier: String {
        switch self {
        case .groupAlreadyContainsProfile: return "PROFILE_ALREADY_IN_GROUP"
        case .groupNotFound: return "GROUP_NOT_FOUND"
        case .groupProfileNotFound: return "MEMBER_NOT_FOUND"
        case .unauthorizedToDeleteGroup: return "UNAUTHORIZED"
        case .unauthorizedToRemoveProfileFromGroup: return "UNAUTHORIZED"
        case .store: return "STORE"
        }
    }

    var reason: String {
        switch self {
        case .groupAlreadyContainsProfile(let groupID, let profileID):
            return "group \(groupID) already contains profile \(profileID)"
        case .groupNotFound(let groupID):
            return "group '\(groupID)' not found"
        case .groupProfileNotFound(let groupID, let profileID):
            return "profile '\(profileID)' not found in group '\(groupID)'"
        case .unauthorizedToDeleteGroup(let groupID, let authenticatedID):
            return "profile \(authenticatedID) not allowed delete group \(groupID)"
        case .unauthorizedToRemoveProfileFromGroup(let profileID, let groupID, authorized: let authenticatedID):
            return "profile \(authenticatedID) not allowed to remove \(profileID) from group \(groupID)"
        case .store(let error):
            return error.localizedDescription
        }
    }
}
