import Vapor
import YouClap

final class MexicoHTTPController: RouterController {
    private let service: MexicoServiceRepresentable

    init(service: MexicoServiceRepresentable) {
        self.service = service
    }

    func boot(router: Router) throws {
        let mexico = router.grouped("viveiro/mexico/group")

        mexico.post(MexicoGroup.self, use: create)
        mexico.delete(String.parameter, use: delete)

        mexico.post(MexicoGroupMember.self, at: "member", use: createMember)
        mexico.delete(String.parameter, "member", String.parameter, use: deleteMember)
    }

    private func create(_ request: Request, mexicoGroup: MexicoGroup) throws -> Future<HTTPStatus> {
        service.create(mexicoGroup).map { _ in HTTPStatus.created }
    }

    private func delete(_ request: Request) throws -> Future<HTTPStatus> {

        let groupFirebaseID = try self.groupFirebaseID(from: request)

        return service.delete(groupFirebaseID)
            .map { _ in HTTPStatus.noContent }
    }

    private func createMember(_ request: Request, mexicoMember: MexicoGroupMember) throws -> Future<HTTPStatus> {
        service.createMember(mexicoMember).map { _ in HTTPStatus.created }
    }

    private func deleteMember(_ request: Request) throws -> Future<HTTPStatus> {

        let groupFirebaseID = try self.groupFirebaseID(from: request)
        guard let memberFirebaseID = try? request.parameters.next(String.self) else {
            throw Abort(.badRequest)
        }

        return service.deleteMember(groupFirebaseID, memberFirebaseID)
            .map { _ in HTTPStatus.noContent }
    }

    private func groupFirebaseID(from request: Request) throws -> String {
        guard let groupFirebaseID = try? request.parameters.next(String.self) else {
            throw Abort(.badRequest)
        }
        return groupFirebaseID
    }
}

extension MexicoServiceError: AbortError {
    var status: HTTPResponseStatus {
        switch self {
        case .store: return .internalServerError
        }
    }

    var identifier: String {
        switch self {
        case .store: return "STORE"
        }
    }

    var reason: String {
        switch self {
        case .store(let error): return error.localizedDescription
        }
    }
}
