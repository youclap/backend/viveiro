import protocol Vapor.Service
import NIO

enum AmericaServiceError: Error {
    case failedToRetrieveCreatorIdentifier(Group.ID)
    case missingCreationDate(Group.ID)
    case store(Error)
}

protocol AmericaServiceRepresentable: Service {
    func create(group: Group, members: [GroupProfile]) -> EventLoopFuture<Void>
    func delete(groupID: Group.ID) -> EventLoopFuture<Void>

    func addMember(groupProfile: GroupProfile, to groupID: Group.ID) -> EventLoopFuture<Void>
    func removeMember(_ profileID: Profile.ID, group groupID: Group.ID) -> EventLoopFuture<Void>
}
