import NIO
import Vapor

enum GroupServiceError: Error {
    case groupAlreadyContainsProfile(Group.ID, Profile.ID)
    case groupNotFound(Group.ID)
    case groupProfileNotFound(Group.ID, Profile.ID)
    case unauthorizedToRemoveProfileFromGroup(Profile.ID, Group.ID, authorized: Profile.ID)
    case unauthorizedToDeleteGroup(Group.ID, Profile.ID)
    case store(Error)
}

protocol GroupServiceRepresentable: Service {
    func create(group: Group.Create) throws -> Future<Group.Response>
    func group(for groupID: Group.ID, authenticatedProfileID: Profile.ID) -> Future<Group.Response>
    func delete(group groupID: Group.ID, authenticatedProfileID: Profile.ID) -> Future<Void>

    func members(groupID: Group.ID, authenticatedProfileID: Profile.ID) -> Future<[Profile.ID]>
    func addMember(groupProfile: GroupProfile.Create,
                   authenticatedProfileID: Profile.ID) -> Future<GroupProfile.Response>
    func removeMember(from groupID: Group.ID,
                      member profileID: Profile.ID,
                      authenticatedProfileID: Profile.ID) -> Future<Void>

    func groups(for profileID: Profile.ID) -> Future<[Group.Response]>
}
