import Vapor
import YouClap

final class GroupService<Store>: GroupServiceRepresentable where Store: GroupStore {

    private let logger: Logger
    private let store: Store
    private let americaService: AmericaServiceRepresentable

    init(logger: Logger, store: Store, americaService: AmericaServiceRepresentable) {
        self.logger = logger
        self.store = store
        self.americaService = americaService
    }

    func create(group: Group.Create) throws -> Future<Group.Response> {
        let newGroup = Group(name: group.name,
                             createdByProfileID: group.createdByProfileID,
                             createdByUserID: group.createdByUserID,
                             firebaseID: Firebase.autoIdentifier)

        let creator = GroupProfile(profileID: group.createdByProfileID,
                                   roleID: GroupProfile.Role.admin.id!, // swiftlint:disable:this force_unwrapping
                                   createdByProfileID: group.createdByProfileID,
                                   createdByUserID: group.createdByUserID)

        let members = group.members
            .filter { $0 != group.createdByProfileID }
            .map { profileID in
                GroupProfile(profileID: profileID,
                             roleID: GroupProfile.Role.member.id!, // swiftlint:disable:this force_unwrapping
                             createdByProfileID: group.createdByProfileID,
                             createdByUserID: group.createdByUserID)
            }

        return store.create(group: newGroup, members: [creator] + members)
            .mapError(GroupServiceError.store)
            .then { group, members in
                self.logger.info("✅ group \(group) created with members \(members)")

                return self.americaService.create(group: group, members: members)
                    .map { _ in group }
            }
            .map(response)
    }

    func group(for groupID: Group.ID, authenticatedProfileID: Profile.ID) -> Future<Group.Response> {
        store.group(for: groupID, authenticatedProfileID: authenticatedProfileID)
            .mapError { error -> GroupServiceError in
                switch error {
                case GroupStoreError.groupNotFound(let groupID):
                    return GroupServiceError.groupNotFound(groupID)
                default:
                    return GroupServiceError.store(error)
                }
            }
            .map {
                guard $0.deleted == false else {
                    throw GroupServiceError.groupNotFound(groupID)
                }

                return $0
            }
            .map(response)
    }

    func delete(group groupID: Group.ID, authenticatedProfileID: Profile.ID) -> Future<Void> {
        store.group(for: groupID, authenticatedProfileID: authenticatedProfileID)
            .flatMap { _ in self.store.role(for: authenticatedProfileID, groupID: groupID) }
            .flatMap { role -> EventLoopFuture<Void> in
                switch role {
                case .admin: return self.store.delete(group: groupID)
                case .member: throw GroupServiceError.unauthorizedToDeleteGroup(groupID, authenticatedProfileID)
                }
            }
            .mapError { error -> GroupServiceError in
                switch error {
                case GroupStoreError.groupNotFound(let groupID):
                    return GroupServiceError.groupNotFound(groupID)
                default:
                    return GroupServiceError.store(error)
                }
            }
            .then { _ in
                self.logger.info("✅ group \(groupID) deleted")

                return self.americaService.delete(groupID: groupID)
            }
    }

    func members(groupID: Group.ID, authenticatedProfileID: Profile.ID) -> Future<[Profile.ID]> {
        store.group(for: groupID, authenticatedProfileID: authenticatedProfileID)
            .flatMap { [store] _ in store.members(groupID: groupID) }
            .mapError { error -> GroupServiceError in
                switch error {
                case GroupStoreError.groupNotFound(let groupID):
                    return GroupServiceError.groupNotFound(groupID)
                default:
                    return GroupServiceError.store(error)
                }
            }
            .map { $0.map { $0.profileID } }
    }

    func addMember(groupProfile: GroupProfile.Create,
                   authenticatedProfileID: Profile.ID) -> Future<GroupProfile.Response> {
        store.group(for: groupProfile.groupID, authenticatedProfileID: authenticatedProfileID)
            .mapError { error -> GroupServiceError in
                switch error {
                case GroupStoreError.groupNotFound(let groupID):
                    return GroupServiceError.groupNotFound(groupID)
                default:
                    return GroupServiceError.store(error)
                }
            }
            .flatMap { self.store.group($0, containsProfileID: groupProfile.profileID) }
            .flatMap { contains -> EventLoopFuture<GroupProfile> in
                guard contains == false else {
                    throw GroupServiceError.groupAlreadyContainsProfile(groupProfile.groupID, groupProfile.profileID)
                }

                let groupProfileToCreate = GroupProfile(groupID: groupProfile.groupID,
                                                        profileID: groupProfile.profileID,
                                                        createdByProfileID: groupProfile.createdByProfileID,
                                                        createdByUserID: groupProfile.createdByUserID)

                return self.store.addMember(groupProfile: groupProfileToCreate)
            }
            .mapError { error -> GroupServiceError in
                switch error {
                case GroupStoreError.groupNotFound(let groupID):
                    return GroupServiceError.groupNotFound(groupID)
                default:
                    return GroupServiceError.store(error)
                }
            }
            .then { createdGroupProfile in
                self.logger.info("✅ member \(groupProfile.profileID) added to group \(groupProfile.groupID)")

                return self.americaService.addMember(groupProfile: createdGroupProfile, to: groupProfile.groupID)
                    .map { _ in createdGroupProfile }
            }
            .map(response)
    }

    func removeMember(from groupID: Group.ID,
                      member profileID: Profile.ID,
                      authenticatedProfileID: Profile.ID) -> Future<Void> {

        store.group(for: groupID, authenticatedProfileID: authenticatedProfileID)
            .and(store.role(for: profileID, groupID: groupID))
            .flatMap {
                let (_, role) = $0

                switch (role, profileID == authenticatedProfileID) {
                case (.admin, _), (_, true):
                    return self.store.removeMember(groupID: groupID, profileID: profileID)
                case (.member, false):
                    throw GroupServiceError
                        .unauthorizedToRemoveProfileFromGroup(profileID, groupID, authorized: authenticatedProfileID)
                }
            }
            .mapError { error -> GroupServiceError in
                switch error {
                case GroupStoreError.groupNotFound(let groupID):
                    return GroupServiceError.groupNotFound(groupID)
                default:
                    return GroupServiceError.store(error)
                }
            }
            .then {
                self.logger.info("✅ member \(profileID) removed from group \(groupID)")

                return self.americaService.removeMember(profileID, group: groupID)
            }
    }

    func groups(for profileID: Profile.ID) -> Future<[Group.Response]> {
        store.groupsForProfileID(profileID: profileID)
    }

    // MARK: - Private Methods

    private func response(for group: Group) throws -> Group.Response {
        try Group.Response(group: group)
    }

    private func response(for groupProfile: GroupProfile) throws -> GroupProfile.Response {
        try GroupProfile.Response(groupProfile: groupProfile)
    }
}
