import GoogleCloudPubSubKit
import YouClap

typealias AmericaGroup = MexicoGroup
typealias AmericaGroupMember = MexicoGroupMember

final class AmericaService<Store>: AmericaServiceRepresentable where Store: AmericaStore & AmericaGroupStore {
    private let store: Store
    private let logger: Logger

    init(store: Store, logger: Logger) {
        self.store = store
        self.logger = logger
    }

    func create(group: Group, members: [GroupProfile]) -> EventLoopFuture<Void> {
        logger.info("Preparing message to create group \(group) with members \(members)")

        return store.firebaseIdentifiers(for: members).flatMap { groupIdentifiers in
            guard let groupProfileIdentifier = groupIdentifiers
                .lazy
                .first(where: { $0.profileID == group.createdByProfileID })
            else {
                throw AmericaServiceError.failedToRetrieveCreatorIdentifier(try group.requireID())
            }

            guard let groupDate = group.createdDate else {
                throw AmericaServiceError.missingCreationDate(try group.requireID())
            }

            let americaGroup = AmericaGroup(creatorID: groupProfileIdentifier.profileFirebaseIdentifier,
                                            name: group.name,
                                            timestamp: groupDate,
                                            uid: groupProfileIdentifier.groupFirebaseIdentifier)

            let americaMessages: [AmericaMessage] = zip(members, groupIdentifiers).compactMap { member, identifier in

                guard let memberCreatedDate = member.createdDate else { return nil }

                let groupMember = AmericaGroupMember(deleted: false,
                                                     groupID: identifier.groupFirebaseIdentifier,
                                                     timestamp: memberCreatedDate,
                                                     userID: identifier.profileFirebaseIdentifier)

                let documentID = "\(identifier.groupFirebaseIdentifier)_\(identifier.profileFirebaseIdentifier)"

                return AmericaMessage(action: .create,
                                      collection: .userGroup,
                                      documentID: documentID,
                                      model: groupMember)
            }

            let americaGroupMessage = AmericaMessage(action: .create,
                                                     collection: .group,
                                                     documentID: groupProfileIdentifier.groupFirebaseIdentifier,
                                                     model: americaGroup)

            return self.store.publish(messages: [americaGroupMessage] + americaMessages)
                .mapError(AmericaServiceError.store)
                .do { [group] in
                    self.logger.info("🎉 successfully sent message to create group \(group) with messages \($0)")
                }
                .map { _ in () }
        }
    }

    func delete(groupID: Group.ID) -> EventLoopFuture<Void> {
        logger.info("Preparing message to delete group \(groupID)")

        return store.firebaseIdentifier(for: groupID)
            .flatMap { firebaseIdentifier -> EventLoopFuture<PublishResponse> in
                let groupDeleteMessage = AmericaMessage(action: .delete,
                                                        collection: .group,
                                                        documentID: firebaseIdentifier)

                return self.store.publish(messages: groupDeleteMessage)
            }
            .mapError(AmericaServiceError.store)
            .do {
                self.logger.info("🎉 successfully sent message to delete group \(groupID) with messages \($0)")
            }
            .map { _ in () }
    }

    func addMember(groupProfile: GroupProfile, to groupID: Group.ID) -> EventLoopFuture<Void> {
        logger.info("Preparing message to add profile \(groupProfile.profileID) to group \(groupID)")

        return store.firebaseIdentifier(for: groupProfile)
            .flatMap { profileGroupIdentifier -> EventLoopFuture<PublishResponse> in

                guard let createdDate = groupProfile.createdDate else {
                    throw AmericaServiceError.missingCreationDate(groupID)
                }

                let groupFirebaseIdentifier = profileGroupIdentifier.groupFirebaseIdentifier
                let profileFirebaseIdentifier = profileGroupIdentifier.profileFirebaseIdentifier

                let documentID = "\(profileFirebaseIdentifier)_\(groupFirebaseIdentifier)"

                let groupMember = AmericaGroupMember(deleted: false,
                                                     groupID: groupFirebaseIdentifier,
                                                     timestamp: createdDate,
                                                     userID: profileFirebaseIdentifier)

                let americaMessage = AmericaMessage(action: .create,
                                                    collection: .userGroup,
                                                    documentID: documentID,
                                                    model: groupMember)

                return self.store.publish(messages: americaMessage)
            }
            .mapError(AmericaServiceError.store)
            .do { _ in self.logger.info("🎉 successfully sent message to add member \(groupProfile) to \(groupID)") }
            .map { _ in () }
    }

    func removeMember(_ profileID: Profile.ID, group groupID: Group.ID) -> EventLoopFuture<Void> {
        logger.info("Preparing message to delete profile \(profileID) from group \(groupID)")

        return store.firebaseIdentifier(for: groupID, profileID: profileID)
            .flatMap { groupProfileIdentifier -> EventLoopFuture<PublishResponse> in
                let groupFirebaseIdentifier = groupProfileIdentifier.groupFirebaseIdentifier
                let profileFirebaseIdentifier = groupProfileIdentifier.profileFirebaseIdentifier

                let documentID = "\(profileFirebaseIdentifier)_\(groupFirebaseIdentifier)"

                let americaMessage = AmericaMessage(action: .delete, collection: .userGroup, documentID: documentID)

                return self.store.publish(messages: americaMessage)
            }
            .mapError(AmericaServiceError.store)
            .do { _ in
                self.logger.info("🎉 successfully sent message to remove member \(profileID) from group \(groupID)")
            }
            .map { _ in () }
    }
}
