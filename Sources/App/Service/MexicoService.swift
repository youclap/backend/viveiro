import Vapor

final class MexicoService<Store>: MexicoServiceRepresentable where Store: GroupStore & MexicoStore {
    private let store: Store

    init(store: Store) {
        self.store = store
    }

    func create(_ mexicoGroup: MexicoGroup) -> Future<Group> {

        let profileIDFuture = store.readProfileID(firebaseID: mexicoGroup.creatorID)
        let userIDFuture = store.readUserID(firebaseID: mexicoGroup.creatorID)

        return profileIDFuture.and(userIDFuture)
            .flatMap { [store] profileID, userID in
                let group = Group(name: mexicoGroup.name,
                                  createdDate: mexicoGroup.timestamp,
                                  createdByProfileID: profileID,
                                  createdByUserID: userID,
                                  privacyID: Group.Privacy.private.id,
                                  firebaseID: mexicoGroup.uid,
                                  deleted: mexicoGroup.deleted)

                let creator = GroupProfile(
                    profileID: group.createdByProfileID,
                    roleID: GroupProfile.Role.admin.id!, // swiftlint:disable:this force_unwrapping
                    createdByProfileID: group.createdByProfileID,
                    createdByUserID: group.createdByUserID)

                return store.create(group: group, members: [creator])
                    .mapError(MexicoServiceError.store)
                    .map { $0.0 }
            }
    }

    func delete(_ groupFirebaseID: String) -> Future<Void> {

        store.readGroup(firebaseID: groupFirebaseID)
            .flatMap { [store] group in
                store.delete(group: try group.requireID())
            }
    }

    func createMember(_ mexicoMember: MexicoGroupMember) -> Future<GroupProfile> {

        store.readGroup(firebaseID: mexicoMember.groupID)
            .flatMap { [store] group in

                let memberProfileIDFuture = store.readProfileID(firebaseID: mexicoMember.userID)

                let addedByProfileIDFuture = store.readProfileID(firebaseID: mexicoMember.addedBy)
                    .catchMap { _ in group.createdByProfileID }
                let addedByUserIDFuture = store.readUserID(firebaseID: mexicoMember.addedBy)
                    .catchMap { _ in group.createdByUserID }

                return memberProfileIDFuture
                    .and(addedByProfileIDFuture)
                    .and(addedByUserIDFuture)
                    .flatMap { [store] in

                        let ((memberProfileID, addedByProfileID), addedByUserID) = $0

                        let groupProfile = GroupProfile(
                            groupID: group.id,
                            profileID: memberProfileID,
                            roleID: GroupProfile.Role.member.id!, // swiftlint:disable:this force_unwrapping
                            createdByProfileID: addedByProfileID,
                            createdByUserID: addedByUserID,
                            createdDate: mexicoMember.timestamp,
                            deleted: mexicoMember.deleted)

                        return store.addMember(groupProfile: groupProfile)
                    }
            }
    }

    func deleteMember(_ groupFirebaseID: String, _ memberFirebaseID: String) -> Future<Void> {

        let groupFuture = store.readGroup(firebaseID: groupFirebaseID)
        let profileIDFuture = store.readProfileID(firebaseID: memberFirebaseID)

        return groupFuture.and(profileIDFuture)
            .flatMap { [store] group, profileID in
                store.removeMember(groupID: try group.requireID(), profileID: profileID)
            }
    }
}
