import Vapor

enum MexicoServiceError: Error {
    case store(Error)
}

protocol MexicoServiceRepresentable: Service {
    func create(_ mexicoGroup: MexicoGroup) -> Future<Group>
    func delete(_ groupFirebaseID: String) -> Future<Void>

    func createMember(_ mexicoMember: MexicoGroupMember) -> Future<GroupProfile>
    func deleteMember(_ groupFirebaseID: String, _ memberFirebaseID: String) -> Future<Void>
}
