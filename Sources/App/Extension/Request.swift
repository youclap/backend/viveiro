import class Vapor.Request
import struct Vapor.Abort
import struct HTTP.HTTPHeaderName
import NIO

extension Request {
    func authenticationID() throws -> String {
        guard let authenticationID = http.headers.firstValue(name: .authenticationID) else {
            throw Abort(.unauthorized)
        }
        return authenticationID
    }

    func profileID() throws -> Profile.ID {
        guard
            let profileIDAsString = http.headers.firstValue(name: .profileID),
            let profileID = Profile.ID(profileIDAsString)
        else {
            throw Abort(.unauthorized)
        }

        return profileID
    }

    func userID() throws -> Profile.ID {
        guard
            let userIDAsString = http.headers.firstValue(name: .userID),
            let userID = User.ID(userIDAsString)
        else {
            throw Abort(.unauthorized)
        }
        return userID
    }
}

extension HTTPHeaderName {
    static let authenticationID = HTTPHeaderName("X-Auth-User")
    static let profileID = HTTPHeaderName("X-Profile-ID")
    static let userID = HTTPHeaderName("X-User-ID")
}
